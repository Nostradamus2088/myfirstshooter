﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public Transform GunPos ;
    public GameObject Bullet;
    public float speed = 100f;
    public float LifeSpan = 1000f;
   
   public void Bullet_Shoot()
   {    
       

       GameObject instBullet = Instantiate(Bullet,GunPos.position,Quaternion.identity) as GameObject;
       Rigidbody instBulletRigidbody = instBullet.GetComponent<Rigidbody>();
       instBulletRigidbody.AddForce(Vector3.forward * speed);

       if(LifeSpan > 0)
       {
           LifeSpan -= Time.deltaTime;
            if(LifeSpan<=0)
            {
                Destroy(instBullet);
            }

       }

       
   }
}
